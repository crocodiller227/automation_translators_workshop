<?php


namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Phrase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class LoadPhraseFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $phrases = [
            'ru' => [
                'Как пройти до библиотеки?',
                'Статья представляет собой обобщение',
                'Хорошо знать родной язык',
                'Хурма была спелой'
            ],
            'sr' => [
                'Како доћи до библиотеке?',
                'Чланак је генерализација',
                'Добро је знати матерњи језик',
                'Персиммон је био зрео',
                ''
            ],
            'cs' => [
                'Jak se dostat do knihovny?',
                'Článek je zobecnění',
                'Je dobré znát svůj rodný jazyk',
                'Persimon byl zralý'
            ],
            'pl' => [
                'Jak dostać się do biblioteki?',
                'Artykuł jest uogólnieniem',
                'Dobrze znać swój ojczysty język',
                'Persimmon był dojrzały'
            ],
            'da' => [
                'Hvordan kommer man til biblioteket?',
                'Artiklen er en generalisering',
                'Det er godt at kende dit modersmål',
                'Persimmon var moden'
            ],
            'hy' => [
                'Ինչպես հասնել գրադարանին:',
                'Հոդվածն ընդհանրացված է',
                'Լավ է իմանալ ձեր մայրենի լեզուն',
                'Պատմությունը հասունացել էր'
            ]
        ];

        foreach ($phrases['ru'] as $key => $phrase) {
            $newPhrase = new Phrase();
            $newPhrase->setDate(new \DateTime());
            $newPhrase->translate('ru')->setPhrase($phrase);
            foreach ($phrases as $phraseIndex => $phrasesArray) {
                if (rand(0, 1)) {
                    $newPhrase->translate($phraseIndex)->setPhrase($phrasesArray[$key]);
                }
                $manager->persist($newPhrase);
                $newPhrase->mergeNewTranslations();
            }
        }
        $manager->flush();
    }
}