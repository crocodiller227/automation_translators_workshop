<?php


namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class LoadUserFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = new User;
        $user
            ->setUsername("root")
            ->setPlainPassword('root')
            ->setEmail("root@root.root")
            ->setEnabled(true)
            ->setRoles(['ROLE_USER']);
        $manager->persist($user);
        $manager->flush();
    }
}