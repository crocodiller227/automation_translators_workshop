<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class PhraseTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $phrase;

    /**
     * @param string $phrase
     * @return PhraseTranslation
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhrase()
    {
        return $this->phrase;
    }

}