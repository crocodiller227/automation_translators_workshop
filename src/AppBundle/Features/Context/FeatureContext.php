<?php

namespace AppBundle\Features\Context;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext  extends MinkContext implements KernelAwareContext

{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When /^я открываю страницу "([^"]*)"$/
     */
    public function ЯОткрываюСтраницу($arg1)
    {
        $this->visit($arg1);
    }

    /**
     * @When /^я вижу форму$/
     */
    public function ЯВижуФорму()
    {
        $this->getSession()->getPage()->find('css', 'form');
    }

    /**
     * @When /^я авторизуюсь, введя логин "([^"]*)" и пароль "([^"]*)"$/
     */
    public function ЯАвторизуюсьВведяЛогинИПароль($arg1, $arg2)
    {
        $this->getSession()->getPage()->find('css', '#username')->setValue($arg1);
        $this->getSession()->getPage()->find('css', '#password')->setValue($arg2);
        $this->getSession()->getPage()->find('css', '#_submit')->click();
    }
    /**
     * @When /^я регистрируюсь, введя email "([^"]*)", usermame "([^"]*)" и пароль "([^"]*)"$/
     */
    public function ЯРегистрируюсьВведяEmailUsernameИПароль($arg1, $arg2, $arg3)
    {
        $this->getSession()->getPage()->find('css', '#fos_user_registration_form_email')->setValue($arg1);
        $this->getSession()->getPage()->find('css', '#fos_user_registration_form_username')->setValue($arg2);
        $this->getSession()->getPage()->find('css', '#fos_user_registration_form_plainPassword_first')->setValue($arg3);
        $this->getSession()->getPage()->find('css', '#fos_user_registration_form_plainPassword_second')->setValue($arg3);
        $this->getSession()->getPage()->find('css', '#_submit')->click();
    }

    /**
     * @When /^я открываю главную страницу приложения с локалью "([^"]*)"$/
     */
    public function ЯОткрываюГлавнуюСтраницуПриложенияСЛокалью($arg1)
    {
        $this->visit(
            $this->kernel
                ->getContainer()->
                get('router')->
                generate('homepage', ['_locale' => $arg1])
        );
    }

    /**
     * @When /^там я вижу форму для добавления фразы на русском языке$/
     */
    public function ЯВижуФормуДобавленияФразыНаРусскомЯзыке(){
        $this->getSession()->getPage()->find('css', 'form');
    }

    /**
     * @When /^я добавляю новую фразу на русском языке "([^"]*)"$/
     */
    public function ЯДобавляюНовуюФразуНаРусскомЯзыке($arg1){
        $this->getSession()->getPage()->find('css', '#form_phrase')->setValue($arg1);
        $this->getSession()->getPage()->find('css', '#form_save')->click();
    }

    /**
     * @When /^я вижу фразу "([^"]*)" где\-то на странице$/
     */
    public function яВижуФразуГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я перехожу на страницу перевода фразы "([^"]*)"$/
     */
    public function ЯПерехожуНаСтраницуПереводаФразы($arg1)
    {
        $this->getSession()->getPage()->findLink($arg1)->click();
    }

    /**
     * @When /^я заполняю поле формы перевода для локали "([^"]*)" фразой "([^"]*)"$/
     */
    public function ЯЗаполняюПолеФормыПереводаДляЛокалиФразой($arg1, $arg2)
    {
        $this->getSession()->getPage()->find('css', "#{$arg1}_lang")->setValue($arg2);
    }

    /**
     * @When /^нажимаю кнопку "([^"]*)"$/
     */
    public function нажимаюКнопку($arg1)
    {
        $this->getSession()->getPage()->find('css', "#{$arg1}")->click();
    }



    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

}
